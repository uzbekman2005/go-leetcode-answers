package answers

import "sort"

func min(num1, num2 int) int {
	if num1 < num2 {
		return num1
	}
	return num2
}

func LongestCommonPrefix_ans(strs []string) string {
	sort.Strings(strs)
	res := 0
	one := strs[0]
	two := strs[len(strs)-1]
	for i := 0; i < min(len(one), len(two)); i++ {
		if one[i] != two[i] {
			break
		}
		res++
	}
	return one[:res]
}
