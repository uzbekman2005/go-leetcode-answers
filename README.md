## go-leetcode-answers

go-leetcode-answer is golang library that you can get answers of leetcode problems.


## Installation

Use go get or go install command

```bash
go get https://gitlab.com/uzbekman2005/go-leetcode-answers/answers.git
```

## Usage

```
import name "https://gitlab.com/uzbekman2005/go-leetcode-answers/answers.git"

#If function should return something just return this function giving needed arguments
# else just call the function with its needed arguments
name.NameOfProblemFunction_ans()
```



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
And make sure that your answer is fast, correct, clear and contain comments if possible.

Functions names should be like this:
```
FunctionNameOfProblem_ans()
```

## Getting started for contributers

```
git clone https://gitlab.com/uzbekman2005/go-leetcode-answers/answers.git

# Open a branch and switch to the branch
git branch -c branch_name 
# open pull request if you are working on one project for long time
git pull
# you should add it staging if you create new go file 
git add filename.go
# To commit the change
git commit filename.go -m "your short comment about what you have changed"
# Create merge request
git push –set-upstream origin your_branch_name

```
